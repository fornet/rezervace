<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class HledaniConstraintValidator extends ConstraintValidator
{
    public function validate(mixed $value, Constraint $constraint): void
    {
        /** @var \App\Form\HledaniData  $value */
        if (!$constraint instanceof HledaniConstraint) {
            throw new UnexpectedTypeException($constraint, HledaniConstraint::class);
        }

        if ($value->getOd() === null || ($value->getDo() === null && $value->getNoci() === null)) {
            $this->context
                ->buildViolation('Zadejte datum příjezdu a počet noci nebo datum příjezdu a datum odjezdu.')
                ->addViolation();
        } elseif ($value->getOd() < (new \DateTime())->setTime(0, 0)) {
            $this->context
                ->buildViolation('Datum příjezdu musí být minimálně dnešní nebo vyšší.')
                ->addViolation();
        } elseif ($value->getNoci() < 1 && $value->getDo() === null) {
            $this->context
                ->buildViolation('Počet nocí musí být vyšší než 1')
                ->addViolation();
        } elseif ($value->getDo() !== null && $value->getOd() > $value->getDo()) {
            $this->context
                ->buildViolation('Datum odjezdu musí být větší než datum příjezdu.')
                ->addViolation();
        }
    }
}
