<?php

namespace App\Validator;

use Symfony\Component\Validator\Attribute\HasNamedArguments;
use Symfony\Component\Validator\Constraint;

#[\Attribute]
class HledaniConstraint extends Constraint
{

    public string $message = 'Zadejte datum příjezdu a počet noci nebo datum příjezdu a datum odjezdu.';
    public string $mode = 'strict';

    #[HasNamedArguments]
    public function __construct(?string $mode = null, ?string $message = null, ?array $groups = null, $payload = null)
    {
        parent::__construct([], $groups, $payload);

        $this->mode = $mode ?? $this->mode;
        $this->message = $message ?? $this->message;
    }

    public function getTargets(): string
    {
        return parent::CLASS_CONSTRAINT;
    }

}
