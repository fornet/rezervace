<?php

namespace App\Form;

use App\Repository\RezervaceRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class HledaniForm extends AbstractType
{
    public function __construct(
        private RezervaceRepository $rezervaceRepository
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $preference = [
            'Jakýkoliv pokoj' => 0,
        ];
        foreach ($this->rezervaceRepository->dostupneTypyPokoju() as $typ) {
            $preference[$typ->getNazev()] = $typ->getId();
        }

        $builder
            ->setMethod('get')
            ->add('typ', ChoiceType::class, ['label' => 'Typ pokoje', 'choices' => $preference])
            ->add('od', DateType::class, ['label' => 'Datum příjezdu', 'required' => false])
            ->add('noci', TextType::class, [
                'label' => 'Počet noci',
                'required' => false,
            ])
            ->add('do', DateType::class, ['label' => 'Datum odjezdu', 'required' => false])

        ;
    }

    public function getBlockPrefix(): string
    {
        return '';
    }
}