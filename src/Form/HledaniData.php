<?php

namespace App\Form;

use App\Validator\HledaniConstraint;
use DateTime;

#[HledaniConstraint]
class HledaniData
{

    protected int $typ = 0;

    protected ?DateTime $od = null;

    protected ?int $noci = null;

    protected ?DateTime $do = null;


    public function getTyp(): int
    {
        return $this->typ;
    }

    public function setTyp(int $typ): static
    {
        $this->typ = $typ;
        $this->recalculate();

        return $this;
    }

    public function getOd(): ?DateTime
    {
        return $this->od;
    }

    public function setOd(?DateTime $od): static
    {
        $this->od = $od;
        $this->recalculate();

        return $this;
    }
    public function getDo(): ?DateTime
    {
        return $this->do;
    }

    public function setDo(?DateTime $do): static
    {
        $this->do = $do;
        $this->recalculate();

        return $this;
    }

    public function getNoci(): ?int
    {
        return $this->noci;
    }

    public function setNoci(?int $noci): static
    {
        $this->noci = $noci;
        $this->recalculate();

        return $this;
    }

    private function recalculate()
    {
        $now = (new DateTime())->setTime(0, 0, 0);
        // podle od / noci
        if ($this->od !== null && $this->noci > 0 && $this->od >= $now) {
            $this->do = (clone $this->od)->modify('+' . $this->noci . ' day');
        }
        // podle od / do
        elseif ($this->od !== null && $this->do !== null && $this->od >= $now && $this->od < $this->do) {
            $this->noci = (int) date_diff($this->od, $this->do)->format('%a');
        }
        // podle noci / do
        elseif ($this->noci > 0 && $this->do !== null) {
            $od = (clone $this->do)->modify('-' . $this->noci . ' day');
            if ($od >= $now) {
                $this->od = $od;
            }
        }
    }
}