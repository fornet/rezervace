<?php

namespace App\Form;

use App\Entity\Pokoj;
use App\Entity\PokojTyp;
use App\Repository\RezervaceRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminPokojDatail extends AbstractType
{

    public function __construct(
        private RezervaceRepository $rezervace
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $deleteDisabled = $builder->getData()->getId() === null || $this->rezervace->jePouzityPokoj($builder->getData());

  
        $builder
            ->add('neaktivni', null, ['label' => 'Neaktivní'])
            ->add('typ', EntityType::class, [
                'class' => PokojTyp::class,
                'choice_label' => 'nazev',
                'label' => 'Typ pokoje'
            ])
            ->add('nazev', null, ['label' => 'Název'])
            ->add('save', SubmitType::class, ['label' => 'Uložit', 'attr' => ['class' => 'btn-primary']])
            ->add('delete', SubmitType::class, ['label' => 'Odstranit', 'disabled' => $deleteDisabled, 'attr' => ['class' => 'btn-danger']])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Pokoj::class,
        ]);
    }

}