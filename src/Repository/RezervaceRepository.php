<?php

namespace App\Repository;

use App\Entity\Pokoj;
use App\Entity\PokojTyp;
use App\Entity\Rezervace;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Rezervace>
 */
class RezervaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Rezervace::class);
    }

    public function jeMoznaRezervace(PokojTyp $pokojTyp = null): bool
    {
        $em = $this->getEntityManager();
        if ($pokojTyp) {
            $aktivni = count($em->getRepository(Pokoj::class)->findBy(['typ' => $pokojTyp, 'neaktivni' => false])) > 0;
        } else {
            $aktivni = count($em->getRepository(Pokoj::class)->findBy(['neaktivni' => false])) > 0;
        }

        return $aktivni;
    }

    public function jeMoznyTermin(DateTime $datumOd, DateTime $datumDo, Pokoj $pokoj = null, PokojTyp $pokojTyp = null)
    {
        $ok = false;

        if ($pokoj && !$pokoj->isNeaktivni()) {
            $res = $this->createQueryBuilder('r')
                ->andWhere('r.pokoj = :pokoj')
                ->andWhere('(r.datumOd <= :od AND r.datumDo > :od) OR (r.datumOd < :do AND r.datumDo >= :do)')
                ->setParameter('pokoj', $pokoj)
                ->setParameter('od', $datumOd)
                ->setParameter('do', $datumDo)
                ->getQuery()
                ->getResult()
            ;
            $ok = empty($res);
        } elseif ($pokojTyp && $this->jeMoznaRezervace($pokojTyp)) {
            foreach ($this->getEntityManager()->getRepository(Pokoj::class)->findBy(['neaktivni' => false, 'typ' => $pokojTyp]) as $r) {
                if ($this->jeMoznyTermin($datumOd, $datumDo, $r)) {
                    $ok = true;
                    break;
                }
            }
        } else {
            foreach ($this->getEntityManager()->getRepository(Pokoj::class)->findBy(['neaktivni' => false,]) as $r) {
                if ($this->jeMoznyTermin($datumOd, $datumDo, $r)) {
                    $ok = true;
                    break;
                }
            }
        }

        return $ok;
    }

    public function nejblizsiTermin(DateTime $datumOd, DateTime $datumDo, PokojTyp $pokojTyp = null): ?array
    {
        $noci = date_diff($datumOd, $datumDo)->format('%a');
        $datumOd = (new DateTime())->setTime(0, 0, 0);
        $datumDo = (clone $datumOd)->modify('+' . $noci . ' day');
        do {
            $ok = $this->jeMoznyTermin($datumOd, $datumDo, null, $pokojTyp);
            if (!$ok) {
                $datumOd->modify('+1 day');
                $datumDo->modify('+1 day');
            }
        } while (!$ok);

        return [
            'datumOd' => $datumOd ,
            'datumDo' => $datumDo,
            'pokojTyp' => $pokojTyp,
        ];
    }

    public function dostupneTypyPokoju(): array
    {
        $em = $this->getEntityManager();
        $typy = [];
        foreach ($em->getRepository(PokojTyp::class)->findAll() as $typ) {
            if ($this->jeMoznaRezervace($typ)) {
                $typy[] = $typ;
            }
        }

        return $typy;
    }

    public function volnePokoje(?PokojTyp $pokojTyp, DateTime $datumOd, DateTime $datumDo): array
    {
        if ($pokojTyp) {
            $pokoje = $this->getEntityManager()->getRepository(Pokoj::class)->findBy(
                ['neaktivni' => false, 'typ' => $pokojTyp],
                ['nazev' => 'ASC']
            );
        } else {
            $pokoje = $this->getEntityManager()->getRepository(Pokoj::class)->findBy(
                ['neaktivni' => false],
                ['nazev' => 'ASC']
            );
        }

        $volne = [];
        foreach ($pokoje as $pokoj) {
            if ($this->jeMoznyTermin($datumOd, $datumDo, $pokoj)) {
                $volne[] = [
                    'pokoj' => $pokoj,
                    'datumOd' => $datumOd,
                    'datumDo' => $datumDo,
                    'noci' => (int) date_diff($datumOd, $datumDo)->format('%a')
                ];
            }
        }

        return $volne;
    }
    public function jePouzityPokoj(Pokoj $pokoj): bool
    {
        return count($this->findBy(['pokoj' => $pokoj]));

    }

}
