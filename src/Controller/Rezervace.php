<?php

namespace App\Controller;

use App\Repository\PokojRepository;
use App\Repository\PokojTypRepository;
use App\Repository\RezervaceRepository;
use App\Form\HledaniData;
use App\Form\HledaniForm;
use App\Form\Rezervace as FormRezervace;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Rezervace as EntityRezervace;
use Doctrine\ORM\EntityManagerInterface;

class Rezervace extends AbstractController
{

    public function __construct(
        private EntityManagerInterface $em,
        private RequestStack $requestStack,
        private PokojTypRepository $pokojTypRepository,
        private PokojRepository $pokojRepository,
        private RezervaceRepository $rezervaceRepository
    ) {
    }

    #[Route("/")]
    public function index(): Response
    {
        return $this->redirectToRoute('rezervace');
    }

    #[Route("/rezervace", name: 'rezervace')]
    public function rezervace(Request $request): Response
    {
        if ($this->rezervaceRepository->jeMoznaRezervace()) {
            $query = $request->query;

            if ($query->get('typ')) {
                $pokojTyp = $this->pokojTypRepository->find((int) $query->get('typ'));
            } else {
                $pokojTyp = null;
            }

            if ($query->get('od')) {
                $datumOd = new DateTime($query->get('od'));
            } else {
                $datumOd = null;
            }

            if ($query->get('od')) {
                $datumDo = new DateTime($query->get('do'));
            } else {
                $datumDo = null;
            }

            if ($query->get('noci')) {
                $noci = (int) $query->get('noci');
            } else {
                $noci = null;
            }

            $hledani = new HledaniData;
            $hledani->setTyp($pokojTyp ? $pokojTyp->getId() : 0);
            $hledani->setDo($datumOd);
            $hledani->setDo($datumDo);
            $hledani->setNoci($noci);

            $form = $this->createForm(
                HledaniForm::class,
                $hledani,
                ['action' => $this->generateUrl('rezervace')]
            );
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $hledani = $form->getData();
                if (
                    $datumOd != $hledani->getOd() ||
                    $datumDo != $hledani->getDo() ||
                    $noci != $hledani->getNoci()
                ) {
                    return $this->redirectToRoute('rezervace', [
                        'typ' => $hledani->getTyp(),
                        'od' => $hledani->getOd()->format('Y-m-d'),
                        'noci' => $hledani->getNoci(),
                        'do' => $hledani->getDo()->format('Y-m-d'),
                    ]);
                }

                $pokoje = $this->rezervaceRepository->volnePokoje($pokojTyp, $datumOd, $datumDo);

                if (empty($pokoje)) {
                    
                    if ($pokojTyp && !$this->rezervaceRepository->jeMoznaRezervace($pokojTyp)) {
                        $neaktivniPokoj = true;
                    } else {
                        $obsazeno = true;
                        if ($dalsiTerminRaw = $this->rezervaceRepository->nejblizsiTermin($datumOd, $datumDo, $pokojTyp)) {
                            $dalsiTermin = [
                                'link' => $this->generateUrl('rezervace', [
                                    'od' => $dalsiTerminRaw['datumOd']->format('Y-m-d'),
                                    'do' => $dalsiTerminRaw['datumDo']->format('Y-m-d'),
                                    'typ' => $dalsiTerminRaw['pokojTyp'] ? $dalsiTerminRaw['pokojTyp']->getId() : 0
                                ]),
                                'text' => $dalsiTerminRaw['datumOd']->format('j.n.Y') . ' - ' . $dalsiTerminRaw['datumDo']->format('j.n.Y')
                            ];
                        }
                    }
                }
            }

            return $this->render('rezervace.html.twig', [
                'form' => $form,
                'pokoje' => $pokoje ?? [],
                'neaktivniPokoj' => $neaktivniPokoj ?? false,
                'obsazeno' => $obsazeno ?? false,
                'dalsiTermin' => $dalsiTermin ?? null,
                'typ' => $hledani->getTyp(),
            ]);
        } else {
            return $this->render('rezervace.html.twig',['neaktivni' => true]);
        }
    }

    #[Route("/rezervace/pokoj", name: 'rezervace-pokoje')]
    public function rezervacePokoj(Request $request): Response
    {

        $query = $request->query;
        $datumOd = new DateTime($query->get('od'));
        $datumDo = new DateTime($query->get('do'));
        $pokoj = $this->pokojRepository->find((int) $query->get('id'));

        $data = [
            'typ' => $query->get('typ'),
            'pokoj' => $pokoj,
            'datumOd' => $datumOd,
            'datumDo' => $datumDo,
            'noci' => date_diff($datumOd, $datumDo)->format('%a'),
        ];

        if ($pokoj->isNeaktivni()) {
            $data['part'] = 'neaktivni';
            $data['nextFree'] = null;
        } elseif (!$this->rezervaceRepository->jeMoznyTermin($datumOd, $datumDo, $pokoj)) {
            $data['part'] = 'obsazeno';
            if ($dalsiTerminRaw = $this->rezervaceRepository->nejblizsiTermin($datumOd, $datumDo, $pokoj->getTyp())) {
                $data['dalsiTermin'] = [
                    'link' => $this->generateUrl('rezervace', [
                        'od' => $dalsiTerminRaw['datumOd']->format('Y-m-d'),
                        'do' => $dalsiTerminRaw['datumDo']->format('Y-m-d'),
                        'typ' => $dalsiTerminRaw['pokojTyp'] ? $dalsiTerminRaw['pokojTyp']->getId() : 0
                    ]),
                    'text' => $dalsiTerminRaw['datumOd']->format('j.n.Y') . ' - ' . $dalsiTerminRaw['datumDo']->format('j.n.Y')
                ];
            }
        } else {
            $rezervace = (new EntityRezervace)
                ->setDatumOd(new DateTime($query->get('od')))
                ->setDatumDo(new DateTime($query->get('do')))
                ->setPokoj($pokoj);

            $form = $this->createForm(
                FormRezervace::class,
                $rezervace,
                [
                    'action' => $this->generateUrl('rezervace-pokoje', [
                        'id' => $query->get('id'),
                        'od' => $query->get('od'),
                        'do' => $query->get('do'),
                        'typ' => $query->get('typ'),
                    ]),
                ]
            );

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $this->em->persist($form->getData());
                $this->em->flush();
                $data['part'] = 'dokonceno';

            } else {
                $data['part'] = 'objednavka';
                $data['form'] = $form;
            }
        }
  
        return $this->render('rezervace-pokoje.html.twig', $data);
    }

}