<?php

namespace App\Controller;

use App\Entity\Pokoj;
use App\Form\AdminPokojDatail;
use App\Repository\PokojRepository;
use App\Repository\RezervaceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class Admin extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $em
    ) {
    }

    #[Route("/admin")]
    public function index(): Response
    {
        return $this->redirectToRoute('admin-rezervace');
    }

    #[Route("/admin/rezervace", name: 'admin-rezervace')]
    public function rezervace(RezervaceRepository $rezervace): Response
    {
        $data = [];
        $data['aktinniMenu'] = 'rezervace';
        $data['nadpis'] = 'Přehled rezervací';
        $data['list'] = $rezervace->findBy([], ['datumOd' => 'DESC']);

        return $this->render('admin-rezervace.html.twig', $data);
    }

    #[Route("/admin/rezervace/{id}", name: 'admin-rezervace-detail')]
    public function rezervaceDetail(RezervaceRepository $rezervace, int $id): Response
    {
        $data = [];
        $data['aktinniMenu'] = 'rezervace';
        $data['nadpis'] = 'Detail rezervace';
        $data['rezervace'] = $rezervace->find($id);

        return $this->render('admin-rezervace-detail.html.twig', $data);
    }

    #[Route("/admin/pokoje", name: 'admin-pokoje')]
    public function pokoje(PokojRepository $pokoje): Response
    {
        $data = [];
        $data['nadpis'] = 'Přehled pokojů';
        $data['aktinniMenu'] = 'pokoje';
        $data['list'] = $pokoje->findBy([], ['nazev' => 'ASC']);

        return $this->render('admin-pokoje.html.twig', $data);
    }

    #[Route("/admin/pokoje/{id}", name: 'admin-pokoje-detail')]
    public function pokojeDetail(
        Request $request,
        PokojRepository $pokoje
        ,
        int $id
    ): Response {
        $data = [];
        $data['aktinniMenu'] = 'pokoje';

        if ($id) {
            $pokoj = $pokoje->find($id);
            $data['nadpis'] = 'Detail pokoje';
        } else {
            $pokoj = new Pokoj();
            ;
            $data['nadpis'] = 'Nový pokoj';
        }

        $form = $this->createForm(
            AdminPokojDatail::class,
            $pokoj,
            ['action' => $this->generateUrl('admin-pokoje-detail', ['id' => $id])]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('save')->isClicked()) {
                $this->em->persist($form->getData());
                $this->em->flush();
                return $this->redirectToRoute('admin-pokoje');
            } elseif ($form->get('delete')->isClicked()) {
                $this->em->remove($form->getData());
                $this->em->flush();
                return $this->redirectToRoute('admin-pokoje');
            }
        }

        $data['form'] = $form;
        return $this->render('admin-pokoje-detail.html.twig', $data);
    }

}