<?php

namespace App\Entity;

use App\Repository\RezervaceRepository;
use App\Rezervace\RezervaceConstraint;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: RezervaceRepository::class)]
class Rezervace
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::INTEGER)]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: Pokoj::class)]
    private Pokoj $pokoj;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private \DateTime $datumOd;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private \DateTime $datumDo;

    #[ORM\Column(type: Types::STRING)]
    #[Assert\NotBlank(
        message: 'Zadejte jméno.',
    )]
    private string $jmeno;

    #[ORM\Column(type: Types::STRING)]
    #[Assert\NotBlank(
        message: 'Zadejte telefon.',
    )]
    private string $telefon;

    #[ORM\Column(type: Types::STRING)]
    #[Assert\NotBlank(
        message: 'Zadejte e-mail'
    )]
    #[Assert\Email(
        message: 'Zadejte e-mail ve správném formátu.',
    )]
    private string $email;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatumOd(): ?\DateTimeInterface
    {
        return $this->datumOd;
    }

    public function setDatumOd(\DateTimeInterface $datumOd): static
    {
        $this->datumOd = $datumOd;

        return $this;
    }

    public function getDatumDo(): ?\DateTimeInterface
    {
        return $this->datumDo;
    }

    public function setDatumDo(\DateTimeInterface $datumDo): static
    {
        $this->datumDo = $datumDo;

        return $this;
    }

    public function getJmeno(): ?string
    {
        return $this->jmeno;
    }

    public function setJmeno(string $jmeno): static
    {
        $this->jmeno = $jmeno;

        return $this;
    }

    public function getTelefon(): ?string
    {
        return $this->telefon;
    }

    public function setTelefon(string $telefon): static
    {
        $this->telefon = $telefon;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getPokoj(): ?Pokoj
    {
        return $this->pokoj;
    }

    public function setPokoj(?Pokoj $pokoj): static
    {
        $this->pokoj = $pokoj;

        return $this;
    }

}
