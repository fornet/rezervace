<?php

namespace App\Entity;

use App\Repository\PokojRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PokojRepository::class)]
class Pokoj
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::INTEGER)]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: PokojTyp::class)] 
    private PokojTyp $typ;

    #[ORM\Column(type: Types::STRING)]
    private string $nazev;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $neaktivni;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNazev(): ?string
    {
        return $this->nazev;
    }

    public function setNazev(string $nazev): static
    {
        $this->nazev = $nazev;

        return $this;
    }

    public function isNeaktivni(): ?bool
    {
        return $this->neaktivni;
    }

    public function setNeaktivni(bool $neaktivni): static
    {
        $this->neaktivni = $neaktivni;

        return $this;
    }

    public function getTyp(): ?PokojTyp
    {
        return $this->typ;
    }

    public function setTyp(?PokojTyp $typ): static
    {
        $this->typ = $typ;

        return $this;
    }
    
}
