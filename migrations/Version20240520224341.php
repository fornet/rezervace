<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240520224341 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE pokoj (id INT AUTO_INCREMENT NOT NULL, typ_id INT DEFAULT NULL, nazev VARCHAR(255) NOT NULL, neaktivni TINYINT(1) NOT NULL, INDEX IDX_57352B68278CD074 (typ_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pokoj_typ (id INT AUTO_INCREMENT NOT NULL, nazev VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rezervace (id INT AUTO_INCREMENT NOT NULL, pokoj_id INT DEFAULT NULL, datum_od DATE NOT NULL, datum_do DATE NOT NULL, jmeno VARCHAR(255) NOT NULL, telefon VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, INDEX IDX_472D00E54666B5 (pokoj_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE pokoj ADD CONSTRAINT FK_57352B68278CD074 FOREIGN KEY (typ_id) REFERENCES pokoj_typ (id)');
        $this->addSql('ALTER TABLE rezervace ADD CONSTRAINT FK_472D00E54666B5 FOREIGN KEY (pokoj_id) REFERENCES pokoj (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pokoj DROP FOREIGN KEY FK_57352B68278CD074');
        $this->addSql('ALTER TABLE rezervace DROP FOREIGN KEY FK_472D00E54666B5');
        $this->addSql('DROP TABLE pokoj');
        $this->addSql('DROP TABLE pokoj_typ');
        $this->addSql('DROP TABLE rezervace');
    }
}
